<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
$app = new App();

$routes = [
    'list' => Controller\ListProducts::class,
    'add' => Controller\AddProduct::class,
    'save' => Controller\SaveProduct::class,
    'delete' => Controller\DeleteProduct::class
];

preg_match('/index\.php\/(\w+)?/i', $_SERVER["REQUEST_URI"], $matches);

$resource = Controller\ListProducts::class;

if (isset($matches[1]) && isset($routes[$matches[1]])) {
    $resource = $routes[$matches[1]];
}

$controller = new $resource($app);
$html = $controller->execute();

echo $html;