<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Domain;

use Model\Domain\Entity\Product;

interface ProductRepositoryInterface
{
    /**
     * @param $field
     * @return bool
     */
    public function exists($field): bool;

    /**
     * @param Product $product
     * @return mixed
     */
    public function addProduct(Product $product);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteProduct(int $id);

    /**
     * @return mixed
     */
    public function getProducts();
}