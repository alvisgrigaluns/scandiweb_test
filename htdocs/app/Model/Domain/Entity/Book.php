<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Domain\Entity;

use Exception;

class Book extends Product
{
    /**
     * @var float|null
     */
    protected ?float $weight;

    /**
     * @param int $id
     * @param string $sku
     * @param string $name
     * @param int $price
     * @param float|null $weight
     * @throws Exception
     */
    public function __construct(int $id, string $sku, string $name, int $price, string $type, float $weight = null)
    {
        parent::__construct($id, $sku, $name, $price, $type);
        $this->setWeight($weight);
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @throws Exception
     */
    public function setWeight(float $weight)
    {
        if (strlen($weight) === 0) {
            throw new Exception('Invalid product weight');
        }

        $this->weight = $weight;
    }
}