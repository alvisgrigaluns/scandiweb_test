<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Controller;

use Exception;
use http\Client\Request;
use Model\Persistence\ProductData;
use Model\Persistence\Repository\ProductMysqlRepository;

class ListProducts extends AbstractRenderer
{
    /**
     * @throws Exception
     */
    protected function prepareTemplate()
    {
        $productRepository = $this->app->mysqlRepository(ProductMysqlRepository::class);
        $products = $productRepository->getProducts();

        /*$productData = new ProductData(
            $products->getSku(),
            $products->getName(),
            $products->getPrice(),
            $products->getType(),
            $products->getWeight(),
            $products->getSize(),
            $products->getHeight(),
            $products->getWidth(),
            $products->getLength()
        );*/

        ob_start();
        include('templates/list.phtml');
        $ob = ob_get_clean();

        return $ob;
    }
}