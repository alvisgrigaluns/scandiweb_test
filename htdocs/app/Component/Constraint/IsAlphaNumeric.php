<?php

namespace Component\Constraint;

use Component\FormValidator;

class IsAlphaNumeric implements Constraint
{
    /**
     * @var string
     */
    private string $inputName;

    /**
     * @var string
     */
    private string $message;

    /**
     * @param string $inputName
     * @param string $message
     */
    public function __construct(string $inputName, string $message)
    {
        $this->inputName = $inputName;
        $this->message = $message;
    }

    /**
     * @param FormValidator $formValidator;
     * @return bool
     */
    public function isFailed(FormValidator $formValidator): bool
    {
        return !preg_match("#\w+#", $formValidator->data()[$this->inputName]);
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}