<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Controller;

class AddProduct extends AbstractRenderer
{
    /**
     * @return false|string
     */
    protected function prepareTemplate()
    {
        ob_start();
        include('templates/add.phtml');
        $ob = ob_get_clean();

        return $ob;
    }
}