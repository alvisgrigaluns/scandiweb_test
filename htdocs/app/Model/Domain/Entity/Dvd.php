<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Domain\Entity;

use Exception;

class Dvd extends Product
{
    /**
     * @var float|null
     */
    protected ?float $size;

    /**
     * @param int $id
     * @param string $sku
     * @param string $name
     * @param int $price
     * @param string $type
     * @param float|null $size
     * @throws Exception
     */
    public function __construct(int $id, string $sku, string $name, int $price, string $type, float $size = null)
    {
        parent::__construct($id, $sku, $name, $price, $type);
        $this->setSize($size);
    }

    /**
     * @return float
     */
    public function getSize(): float
    {
        return $this->size;
    }

    /**
     * @param float $size
     * @throws Exception
     */
    public function setSize(float $size)
    {
        if (strlen($size) === 0) {
            throw new Exception('Invalid product size');
        }

        $this->size = $size;
    }
}