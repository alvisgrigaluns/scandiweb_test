<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Controller;

use App;

abstract class AbstractRenderer
{
    /**
     * @var App
     */
    protected App $app;

    /**
     * @var array
     */
    protected array $data = [];

    /**
     * @return mixed
     */
    abstract protected function prepareTemplate();

    /**
     * @param App $app
     * @param array $data
     */
    public function __construct(App $app, array $data = []) {
        $this->data = $data;
        $this->app = $app;
    }

    /**
     * @return mixed
     */
    public function execute() {
        return $this->prepareTemplate();
    }
}