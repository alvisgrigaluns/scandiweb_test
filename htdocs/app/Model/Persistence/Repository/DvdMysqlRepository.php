<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Persistence\Repository;

use Exception;
use Model\Domain\DvdRepositoryInterface;
use Model\Domain\Entity\Dvd;
use Model\Persistence\DvdData;
use PDO;

class DvdMysqlRepository implements DvdRepositoryInterface
{
    /**
     * @var PDO
     */
    private PDO $PDO;

    /**
     * @param PDO $PDO
     */
    public function __construct(PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    /**
     * @return array|DvdData[]
     */
    public function getDvds(): array
    {
        $query = $this->PDO->query('SELECT * FROM dvd');
        $dvds = [];
        foreach ($query->fetchAll() as $row) {
            $dvds[] = new DvdData(
                $row['size']
            );
        }

        return $dvds;
    }

    /**
     * @param $id
     */
    public function getDvdId($id)
    {
        $query = $this->PDO->prepare('SELECT * FROM dvd WHERE id = ?');
        $query->bindParam('d', $id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param Dvd $cd
     * @return void
     */
    public function addDvd(Dvd $cd)
    {
        $query = $this->PDO->prepare('INSERT INTO cd (sku, name, price, size)VALUE(:sku, :name, :price, :size)');
        $query->execute(array(
            ':sku' => $cd->getSku(),
            ':name' => $cd->getName(),
            ':price' => $cd->getPrice(),
            ':size' => $cd->getSize(),
        ));
    }

    /**
     * @param int $id
     */
    public function deleteDvd(int $id)
    {
        $query = $this->PDO->prepare('DELETE FROM cd WHERE id = ?');
        $query->bindParam('id', $id, PDO::PARAM_INT);
        $query->execute();
    }
}