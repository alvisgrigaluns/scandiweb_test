<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Persistence\Repository;

use Exception;
use Model\Domain\Entity\Furniture;
use Model\Domain\FurnitureRepositoryInterface;
use Model\Persistence\FurnitureData;
use PDO;

class FurnitureMysqlRepository implements FurnitureRepositoryInterface
{
    /**
     * @var PDO
     */
    private PDO $PDO;

    /**
     * @param PDO $PDO
     */
    public function __construct(PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    /**
     * @return array|FurnitureData[]
     */
    public function getFurniture(): array
    {
        $query = $this->PDO->query('SELECT * FROM furniture');
        $furniture = [];
        foreach ($query->fetchAll() as $row) {
            $furniture[] = new FurnitureData(
                $row['height'],
                $row['width'],
                $row['length'],
            );
        }

        return $furniture;
    }

    /**
     * @param $id
     */
    public function getFurnitureId($id)
    {
        $query = $this->PDO->prepare('SELECT * FROM furniture WHERE id = ?');
        $query->bindParam('d', $id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param Furniture $furniture
     */
    public function addFurniture(Furniture $furniture)
    {
        $query = $this->PDO->prepare('INSERT INTO furniture (sku, name, price, height, width, length )VALUE(:sku, :name, :price, :height, :width, :length)');
        $query->execute(array(
            ':sku' => $furniture->getSku(),
            ':name' => $furniture->getName(),
            ':price' => $furniture->getPrice(),
            ':height' => $furniture->getHeight(),
            ':width' => $furniture->getWidth(),
            ':length' => $furniture->getLength()
        ));
    }

    /**
     * @param int $id
     */
    public function deleteFurniture(int $id)
    {
        $query = $this->PDO->prepare('DELETE FROM furniture WHERE id = ?');
        $query->bindParam('id', $id, PDO::PARAM_INT);
        $query->execute();
    }
}