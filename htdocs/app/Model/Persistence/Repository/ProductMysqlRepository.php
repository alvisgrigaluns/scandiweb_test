<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Persistence\Repository;

use Component\Constraint\UniqueConstraint;
use Model\Domain\Entity\Product;
use Model\Domain\ProductRepositoryInterface;
use Model\Persistence\ProductData;
use PDO;

class ProductMysqlRepository implements ProductRepositoryInterface, UniqueConstraint
{
    /**
     * @var PDO
     */
    private PDO $PDO;

    /**
     * @param PDO $PDO
     */
    public function __construct(PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    /**
     * @param $sku
     * @return bool
     */
    public function exists($sku): bool
    {
        $query = $this->PDO->prepare('SELECT sku FROM product WHERE sku = :sku');
        $query->execute([':sku' => $sku]);

        return !empty($query->fetch());
    }

    /**
     * @return array|ProductData[]
     */
    public function getProducts(): array
    {
        $query = $this->PDO->query('
            SELECT 
                product.id, 
                product.sku,
                product.name,
                product.price,
                product.type,
                book.id,
                book.weight,
                dvd.id,
                dvd.size,
                furniture.id,
                furniture.height,
                furniture.width,
                furniture.length
            FROM product
            LEFT JOIN book
            ON book.id = product.id
            LEFT JOIN dvd
            ON dvd.id = product.id
            LEFT JOIN furniture
            ON furniture.id = product.id
            ORDER BY product.id
            ');
        $products = [];
        foreach ($query->fetchAll() as $row) {
            $products[] = new ProductData(
                $row['sku'],
                $row['name'],
                $row['price'],
                $row['type'],
                $row['weight'],
                $row['size'],
                $row['height'],
                $row['width'],
                $row['length'],
            );
        }

        return array($products);
    }

    /**
     * @param $id
     */
    public function getProductId($id)
    {
        $query = $this->PDO->prepare('SELECT * FROM product WHERE id = ?');
        $query->bindParam('d', $id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $query = $this->PDO->prepare('INSERT INTO product (sku, name, price, type)VALUE(:sku, :name, :price, :type)');
        $query->execute(array(
            ':sku' => $product->getSku(),
            ':name' => $product->getName(),
            ':price' => $product->getPrice(),
            ':type' => $product->getType()
        ));
    }

    /**
     * @param int $id
     */
    public function deleteProduct(int $id)
    {
        $query = $this->PDO->prepare('DELETE FROM product WHERE id = ?');
        $query->bindParam('id', $id, PDO::PARAM_INT);
        $query->execute();
    }
}