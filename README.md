# Junior Test Assignment

### Database Configuration

1. Mysql config
   - Rename **database.template.php** to **database.php** and put your database information there.
   - Configure as well your **docker-compose.yaml**.

2. Docker config
   - Rename **.env.template** to **.env** and put your database information.

3. Run docker-compose file
   - > docker-compose up --build