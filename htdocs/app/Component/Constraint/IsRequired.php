<?php

namespace Component\Constraint;

use Component\FormValidator;

class IsRequired implements Constraint
{
    /**
     * @var string
     */
    private string $inputName;
    /**
     * @var string
     */
    private string $message;


    public function __construct(string $inputName, string $message)
    {
        $this->inputName = $inputName;
        $this->message = $message;
    }

    /**
     * @param FormValidator $formValidator
     * @return bool
     */
    public function isFailed(FormValidator $formValidator): bool
    {
        return strlen($formValidator->data()[$this->inputName]) === 0;
    }
    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}