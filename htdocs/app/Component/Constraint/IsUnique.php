<?php

namespace Component\Constraint;

use Component\FormValidator;

class IsUnique implements Constraint
{
    /**
     * @var string
     */
    private string $inputName;
    /**
     * @var string
     */
    private string $message;

    /**
     * @var UniqueConstraint
     */
    private UniqueConstraint $UniqueConstraint;

    /**
     * IsUnique constructor.
     * @param string $inputName
     * @param string $message
     * @param UniqueConstraint $UniqueConstraint
     */
    public function __construct(string $inputName, string $message, UniqueConstraint $UniqueConstraint)
    {
        $this->inputName = $inputName;
        $this->message = $message;
        $this->UniqueConstraint = $UniqueConstraint;
    }

    /**
     * @param FormValidator $formValidator
     * @return bool
     */
    public function isFailed(FormValidator $formValidator): bool
    {
        return $this->UniqueConstraint->exists($formValidator->data()[$this->inputName]);
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}