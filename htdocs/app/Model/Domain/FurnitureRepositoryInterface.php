<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Domain;

use Model\Domain\Entity\Furniture;

interface FurnitureRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getFurniture();

    /**
     * @param Furniture $furniture
     * @return mixed
     */
    public function addFurniture(Furniture $furniture);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteFurniture(int $id);
}