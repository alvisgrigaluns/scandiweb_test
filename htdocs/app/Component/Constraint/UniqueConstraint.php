<?php

namespace Component\Constraint;

interface UniqueConstraint
{
    /**
     * @param $field
     * @return bool
     */
    public function exists($field): bool;
}