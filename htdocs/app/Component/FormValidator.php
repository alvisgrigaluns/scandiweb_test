<?php

namespace Component;

use Component\Constraint\Constraint;

class FormValidator
{
    const VALIDATOR_SESSION = "form_validation";

    /**
     * @var array
     */
    private array $constraints = [];

    /**
     * @var array
     */
    private array $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return $this->data;
    }

    /**
     * @param $key
     * @param Constraint $constraint
     * @return void
     */
    public function attach($key, Constraint $constraint)
    {
        $this->constraints[$key] = $constraint;
    }

    /**
     * @return array|Constraint[]
     */
    public function constraints(): array
    {
        return $this->constraints;
    }

    /**
     * @param $code
     * @return mixed
     */
    public static function flashMessage($code)
    {
        if(!empty($_SESSION[self::VALIDATOR_SESSION][$code])) {
            $validationMessage = $_SESSION[self::VALIDATOR_SESSION][$code];
            unset($_SESSION[self::VALIDATOR_SESSION][$code]);

            return $validationMessage;
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public function isFailed(string $key): bool
    {
        foreach ($this->constraints as $messageCode => $constraint) {
            if ($constraint->isFailed($this)) {
                $_SESSION[self::VALIDATOR_SESSION][$messageCode] = $constraint->message();
            }
        }

        return !empty($_SESSION[self::VALIDATOR_SESSION]);
    }
}