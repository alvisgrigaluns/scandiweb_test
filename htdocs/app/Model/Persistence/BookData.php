<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Persistence;

class BookData
{
    /**
     * @var float
     */
    public float $weight;

    /**
     * @param float $weight
     */
    public function __construct(float $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }
}