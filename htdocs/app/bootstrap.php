<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
require_once "config/env.php";
require_once "config/database/database.php";

if (APP_DEBUG === true) {
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
}

session_start();

spl_autoload_register('AutoLoader');

function AutoLoader($className) {
    $file = str_replace('\\',DIRECTORY_SEPARATOR, $className);

    require_once 'app' . DIRECTORY_SEPARATOR . $file . '.php';
}

try {
    require_once "routes.php";

} catch (PDOException $exception) {
    if (APP_DEBUG === true) {
        echo '<pre>' . var_export($exception, true) . '</pre>';
    } else {
        echo '<h2>Whoops, something went wrong!</h2>';
    }
    file_put_contents("/config/exceptions/error.log",
        $exception->getMessage() . "\n" .
        $exception->getTraceAsString(),
        FILE_APPEND);
}
