<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Persistence;

class ProductData
{
    /**
     * @var string
     */
    public string $sku;

    /**
     * @var string
     */
    public string $name;

    /**
     * @var float
     */
    public float $price;

    /**
     * @var string
     */
    public string $type;

    /**
     * @var float|null
     */
    public ?float $weight;

    /**
     * @var float|null
     */
    public ?float $size;

    /**
     * @var float|null
     */
    public ?float $height;

    /**
     * @var float|null
     */
    public ?float $width;

    /**
     * @var float|null
     */
    public ?float $length;

    /**
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param string $type
     * @param float|null $weight
     * @param float|null $size
     * @param float|null $height
     * @param float|null $width
     * @param float|null $length
     */
    public function __construct(string $sku, string $name, float $price, string $type, float $weight = null, float $size = null, float $height = null, float $width = null, float $length = null)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
        $this->weight = $weight;
        $this->size = $size;
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @return float|null
     */
    public function getSize(): ?float
    {
        return $this->size;
    }

    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }
}