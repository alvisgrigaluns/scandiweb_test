<?php

/**
 * Database host connection
 */
const DB_HOST = 'mysql:host=your_host;dbname=your_database';

/**
 * Database user
 */
const DB_USER = 'your_user';

/**
 * Database password
 */
const DB_PASSWORD = 'your_password';

/**
 * Database option
 */
const DB_OPTIONS = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];