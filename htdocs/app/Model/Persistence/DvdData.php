<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Persistence;

class DvdData
{
    /**
     * @var float
     */
    public float $size;

    /**
     * @param float $size
     */
    public function __construct(float $size)
    {
        $this->size = $size;
    }

    /**
     * @return float
     */
    public function getSize(): float
    {
        return $this->size;
    }
}