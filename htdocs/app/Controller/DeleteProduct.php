<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Controller;

use Exception;
use Model\Domain\Entity\ProductSKU;
use Model\Persistence\Repository\ProductMysqlRepository;

class DeleteProduct extends AbstractController
{
    /**
     * @throws Exception
     */
    public function execute()
    {
        $productRepository = $this->app->mysqlRepository(ProductMysqlRepository::class);

        if (isset($_POST['checkbox']) && isset($_POST['delete_btn'])) {
            $productId = new ProductSKU($_GET['id']);
            $productRepository->deleteProduct($productId);
        }
        header('location:/');

        return $productRepository;
    }
}