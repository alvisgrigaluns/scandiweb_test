<?php

namespace Component\Constraint;

use Component\FormValidator;

interface Constraint
{
    /**
     * @param FormValidator $formValidator
     * @return bool
     */
    public function isFailed(FormValidator $formValidator): bool;

    /**
     * @return string
     */
    public function message(): string;
}