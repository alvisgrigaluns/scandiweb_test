<?php

namespace Model\Domain\Entity;

use Exception;
use Model\Domain\ProductRepositoryInterface;

final class ProductSKU
{
    /**
     * @param string $sku
     * @param ProductRepositoryInterface $productRepository
     * @throws Exception
     */
    public function __construct(string $sku, ProductRepositoryInterface $productRepository)
    {
        $this->setSku($sku, $productRepository);
    }

    /**
     * @param string $sku
     * @param ProductRepositoryInterface $productRepository
     * @throws Exception
     */
    private function setSku(string $sku, ProductRepositoryInterface $productRepository)
    {
        if (strlen($sku) === 0 || $productRepository->exists($sku)) {
            throw new Exception("Invalid sku");
        }

        $this->sku = $sku;
    }
}