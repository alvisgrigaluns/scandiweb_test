<?php

namespace Controller;

use App;

abstract class AbstractController
{
    /**
     * @var App
     */
    protected App $app;

    /**
     * Controller constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * @return mixed
     */
    public abstract function execute();
}