<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Persistence\Repository;

use Exception;
use Model\Domain\BookRepositoryInterface;
use Model\Domain\Entity\Book;
use Model\Persistence\BookData;
use PDO;
use PDOStatement;

class BookMysqlRepository implements BookRepositoryInterface
{
    /**
     * @var PDO
     */
    private PDO $PDO;

    /**
     * @param PDO $PDO
     */
    public function __construct(PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    /**
     * @return array|BookData[]
     */
    public function getBooks(): array
    {
        $query = $this->PDO->query('SELECT * FROM book');
        $books = [];
        foreach ($query->fetchAll() as $row) {
            $books[] = new BookData(
                $row['weight']
            );
        }

        return $books;
    }

    /**
     * @param $id
     */
    public function getBookId($id)
    {
        $query = $this->PDO->prepare('SELECT * FROM book WHERE id = ?');
        $query->bindParam('d', $id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param Book $book
     * @return void
     */
    public function addBook(Book $book)
    {
        $query = $this->PDO->prepare('INSERT INTO book (sku, name, price, weight)VALUE(:sku, :name, :price, :weight)');
        $query->execute(array(
            ':sku' => $book->getSku(),
            ':name' => $book->getName(),
            ':price' => $book->getPrice(),
            ':weight' => $book->getWeight(),
        ));
    }

    /**
     * @param int $id
     * @return false|PDOStatement
     */
    public function deleteBook(int $id)
    {
        $query = $this->PDO->prepare('DELETE FROM book WHERE id = ?');
        $query->bindParam('id', $id, PDO::PARAM_INT);
        $query->execute();

        return $query;
    }
}