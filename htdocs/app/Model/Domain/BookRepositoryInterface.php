<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Domain;

use Model\Domain\Entity\Book;

interface BookRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getBooks();

    /**
     * @param Book $book
     * @return void
     */
    public function addBook(Book $book);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteBook(int $id);
}