<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Persistence;

class FurnitureData
{
    /**
     * @var float
     */
    public float $height;

    /**
     * @var float
     */
    public float $width;

    /**
     * @var float
     */
    public float $length;

    /**
     * @param float $height
     * @param float $width
     * @param float $length
     */
    public function __construct(float $height, float $width, float $length)
    {
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     * @return float
     */
    public function getLength(): float
    {
        return $this->length;
    }
}