<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Domain\Entity;

use Exception;
use Model\Domain\ProductRepositoryInterface;

class Product
{
    /**
     * @var int
     */
    protected int $id;

    /**
     * @var string
     */
    protected string $sku;

    /**
     * @var string
     */
    protected string $name;

    /**
     * @var float
     */
    protected float $price;

    /**
     * @var string
     */
    protected string $type;

    /**
     * @param int $id
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param string $type
     */
    public function __construct(int $id, string $sku, string $name, float $price, string $type)
    {
        $this->id = $id;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     * @param ProductRepositoryInterface $productRepository
     * @throws Exception
     */
    public function setSku(string $sku, ProductRepositoryInterface $productRepository)
    {
        if (strlen($sku) === 0 || $productRepository->exists($sku)) {
            throw new Exception("Invalid product sku");
        }

        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @throws Exception
     */
    public function setName(string $name)
    {
        if (strlen($name) === 0) {
            throw new Exception("Invalid product name");
        }

        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @throws Exception
     */
    public function setPrice(float $price)
    {
        if (strlen($price) === 0) {
            throw new Exception('Invalid product price');
        }

        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @throws Exception
     */
    public function setType(string $type)
    {
        if (strlen($type) === 0) {
            throw new Exception('Invalid product type');
        }

        $this->type = $type;
    }
}
