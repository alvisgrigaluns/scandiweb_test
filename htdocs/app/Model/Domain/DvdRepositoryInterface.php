<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Domain;

use Model\Domain\Entity\Dvd;

interface DvdRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getDvds();

    /**
     * @param Dvd $dvd
     * @return void
     */
    public function addDvd(Dvd $dvd);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteDvd(int $id);
}