<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Model\Domain\Entity;

use Exception;

class Furniture extends Product
{
    /**
     * @var float|null
     */
    protected ?float $height;

    /**
     * @var float|null
     */
    protected ?float $width;

    /**
     * @var float|null
     */
    protected ?float $length;

    /**
     * @param int $id
     * @param string $sku
     * @param string $name
     * @param int $price
     * @param string $type
     * @param float|null $height
     * @param float|null $width
     * @param float|null $length
     * @throws Exception
     */
    public function __construct(int $id, string $sku, string $name, int $price, string $type, float $height = null, float $width = null, float $length = null)
    {
        parent::__construct($id, $sku, $name, $price, $type);
        $this->setHeight($height);
        $this->setWidth($width);
        $this->setLength($length);
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    /**
     * @param float $height
     * @throws Exception
     */
    public function setHeight(float $height)
    {
        if (strlen($height) === 0) {
            throw new Exception('Invalid product height');
        }

        $this->height = $height;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     * @param float $width
     * @throws Exception
     */
    public function setWidth(float $width)
    {
        if (strlen($width) === 0) {
            throw new Exception('Invalid product width');
        }

        $this->width = $width;
    }

    /**
     * @return float
     */
    public function getLength(): float
    {
        return $this->length;
    }

    /**
     * @param float $length
     * @throws Exception
     */
    public function setLength(float $length)
    {
        if (strlen($length) === 0) {
            throw new Exception('Invalid product length');
        }

        $this->length = $length;
    }
}