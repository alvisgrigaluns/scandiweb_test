<?php

final class App
{
    /**
     * @return PDO
     */
    public final function getDatabase(): PDO
    {
        if(!isset($this->database)) {
            $pdo = new PDO(DB_HOST, DB_USER, DB_PASSWORD, DB_OPTIONS);

            $this->database = $pdo;
        }

        return $this->database;
    }

    /**
     * @param string $instance
     * @return mixed
     */
    public function mysqlRepository(string $instance)
    {
        return new $instance($this->getDatabase());
    }
}