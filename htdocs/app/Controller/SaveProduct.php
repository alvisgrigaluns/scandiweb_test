<?php
/**
 * @Author Alvis Grigaļūns <alvis.grigaluns@gmail.com>
 */
namespace Controller;

use Component\Constraint\IsAlphaNumeric;
use Component\Constraint\IsRequired;
use Component\Constraint\IsUnique;
use Component\FormValidator;
use Exception;
use Model\Domain\Entity\Product;
use Model\Domain\Entity\ProductSKU;
use Model\Persistence\Repository\ProductMysqlRepository;

class SaveProduct extends AbstractController
{
    /**
     * @throws Exception
     */
    public function execute()
    {
        $productRepository = $this->app->mysqlRepository(ProductMysqlRepository::class);

        $formValidator = new FormValidator($_POST);
        $formValidator->attach("sku_must_be_unique", new IsUnique("sku", "SKU must be unique!", $productRepository));
        $formValidator->attach("sku_is_required", new IsRequired("sku", "SKU cannot be empty!"));
        $formValidator->attach("sku_must_be_alpha_numeric", new IsAlphaNumeric("sku", "SKU must contain only alpha numeric characters!"));
        $formValidator->attach("name_is_required", new IsRequired("name", "Name cannot be empty!"));
        $formValidator->attach("price_is_required", new IsRequired("price", "Price cannot be empty!"));
        $formValidator->attach("weight_is_required", new IsRequired("weight", "Weight cannot be empty!"));
        $formValidator->attach("size_is_required", new IsRequired("size", "Size cannot be empty!"));
        $formValidator->attach("height_is_required", new IsRequired("height", "Height cannot be empty!"));
        $formValidator->attach("width_is_required", new IsRequired("width", "Width cannot be empty!"));
        $formValidator->attach("length_is_required", new IsRequired("length", "Length cannot be empty!"));

        try {
            $product = new Product(
                new ProductSKU($_POST['sku']),
                $_POST['name'],
                $_POST['price'],
            );
            $productRepository->addProduct($product);

            header("location: /");

        } catch (Exception $exception) {
            throw new Exception("Cant save this product", 0, $exception);
        }

        return $product;
    }
}